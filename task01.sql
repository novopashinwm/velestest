﻿
if (not exists(select * from sysobjects where name = 'Departs'))
begin
	CREATE TABLE dbo.Departs
	(
	  [Id]        Int           NOT NULL,
	  [Name]      NVarChar(250)  NOT NULL,   -- Название отдела
	  CONSTRAINT [PK_Departs] PRIMARY KEY CLUSTERED ([Id])
	)
end;
go
if (not exists(select * from sysobjects where name = 'Persons'))
begin
	CREATE TABLE dbo.Persons
	(
	  [Id]        Int           NOT NULL,
	  [Name]      NVarChar(250)  NOT NULL,   -- Имя сотрудника
	  [Depart_Id] Int               NULL,   -- Ссылка на Departs
	  CONSTRAINT [PK_Persons] PRIMARY KEY CLUSTERED ([Id]),
	  CONSTRAINT [FK_Persons_Departs]
	  FOREIGN KEY ([Depart_Id]) REFERENCES dbo.Departs ([Id])
	)
end;
go
if (not exists(select  * from dbo.Departs))
begin
	insert dbo.Departs values (1,N'ААА'),(2,N'БББ'),(3,N'ВВВ')
end 
go
if (not exists(select  * from dbo.Persons))
begin
	insert dbo.Persons 
	values 
	(1, N'Иванов', 1) 
   ,(2, N'Петров', 1)
   ,(4, N'Сидоров', 2)
   ,(5, N'Антонов', 2)
   ,(7, N'Бубонов', 2)

end 
go

/*Написать запрос, который будет возвращать список сотрудников с указанием 
для каждого сотрудника отдела, в котором данный сотрудник работает. 
Т.е. результат должен содержать два поля: PersonName, DepartName.*/

select pers.Name as PersonName, dep.Name as DepartName
from
dbo.Persons pers 
join dbo.Departs dep on pers.Depart_Id = dep.Id
order by PersonName