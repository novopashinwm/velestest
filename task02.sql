﻿/*По структуре А.
Написать запрос, который будет возвращать список отделов с указанием для каждого отдела 
кол-ва сотрудников в данном отделе. 
Т.е. результат должен содержать два поля: DepartName, PersonsCount.
*/
select dep.Name as DepartName, 
IsNull((select count(*) from dbo.Persons pers where pers.Depart_Id = dep.Id),0) as PersonsCount
from
dbo.Departs dep